﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text.Json;

namespace Flt.Mobi4Me.CoreBankMock.Api.Controllers
{
    [ApiController]
    public class MockController : ControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public MockController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("[action]")]
        [ActionName("mobile.ListBranches")]
        public ActionResult BranchList()
        {
            var path = Path.Combine(_webHostEnvironment.ContentRootPath, "JsonResponses");
            var json = System.IO.File.ReadAllText(Path.Combine(path, "branches.json"));
            var myJObject = JsonSerializer.Deserialize<object>(json: json);
            return Ok(myJObject);
        }

        [HttpPost("[action]")]
        [ActionName("mobile.LookupAccountTransactions")]
        public ActionResult LookupAccountTransactions([FromBody] AccountTransactionsCoreBankRequest request)
        {
            var statusCode = ShouldThrowError(request.AccountId, out var error);
            if (statusCode > 0)
            {
                return StatusCode(statusCode, error);
            }

            var path = Path.Combine(_webHostEnvironment.ContentRootPath, "JsonResponses");
            var json = System.IO.File.ReadAllText(Path.Combine(path, "accountTransactions.json"));
            var myJObject = JsonSerializer.Deserialize<object>(json: json);
            return Ok(myJObject);
        }

        [HttpPost("[action]")]
        [ActionName("mobile.LookupLoanAccount")]
        public ActionResult LookupLoanAccount([FromBody] LoanAccountCoreBankRequest request)
        {
            var statusCode = ShouldThrowError(request.AccountId, out var error);
            if (statusCode > 0)
            {
                return StatusCode(statusCode, error);
            }

            var path = Path.Combine(_webHostEnvironment.ContentRootPath, "JsonResponses");
            var json = System.IO.File.ReadAllText(Path.Combine(path, "loanAccount.json"));
            var myJObject = JsonSerializer.Deserialize<object>(json: json);
            return Ok(myJObject);
        }

        [HttpPost("[action]")]
        [ActionName("mobile.LookupDepositAccount")]
        public ActionResult LookupDepositAccount([FromBody] DepositAccountCoreBankRequest request)
        {
            var statusCode = ShouldThrowError(request.AccountId, out var error);
            if (statusCode > 0)
            {
                return StatusCode(statusCode, error);
            }

            var path = Path.Combine(_webHostEnvironment.ContentRootPath, "JsonResponses");
            var json = System.IO.File.ReadAllText(Path.Combine(path, "depositAccount.json"));
            var myJObject = JsonSerializer.Deserialize<object>(json: json);
            return Ok(myJObject);
        }

        [HttpPost("[action]")]
        [ActionName("mobile.LookupClient")]
        public ActionResult LookupClient([FromBody] ClientCoreBankRequest request)
        {
            var statusCode = ShouldThrowError(request.PhoneNumber, out var error);
            if (statusCode > 0)
            {
                return StatusCode(statusCode, error);
            }

            var path = Path.Combine(_webHostEnvironment.ContentRootPath, "JsonResponses");
            var json = System.IO.File.ReadAllText(Path.Combine(path, "client.json"));
            var myJObject = JsonSerializer.Deserialize<dynamic>(json: json);
            return Ok(myJObject);
        }

        private int ShouldThrowError(string accountId, out dynamic error)
        {
            error = "Error";
            if (int.TryParse(accountId, out int statusCode) && statusCode < 600)
            {
                if (statusCode == 400)
                {
                    error = new System.Dynamic.ExpandoObject();
                    error.FieldRef = "Some field";
                    error.Message = "Text is too long";
                }

                return statusCode;
            }

            return 0;
        }
    }
}
