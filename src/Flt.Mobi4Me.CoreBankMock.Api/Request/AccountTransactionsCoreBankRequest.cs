﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.CoreBankMock.Api
{
    public class AccountTransactionsCoreBankRequest
    {
        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }

        [JsonPropertyName("beforeTransactionID")]
        public string BeforeTransactionId { get; set; }
    }
}
