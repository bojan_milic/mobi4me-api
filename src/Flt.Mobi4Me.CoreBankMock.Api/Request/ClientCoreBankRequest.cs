﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.CoreBankMock.Api
{
    public class ClientCoreBankRequest
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("phone")]
        public string PhoneNumber { get; set; }
    }
}
