﻿using System;

namespace Flt.Mobi4Me.Guards
{
    /// <summary>
    /// Represents a set of static guard helper methods for guarding against inclement situations
    /// </summary>
    public static class Guard
    {
        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> if <paramref name="value"/> is null
        /// </summary>
        /// <param name="value">The value to test for null</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstNull(object value, string paramName, string message = null)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName, message ?? $"The object passed for parameter {paramName} was found to be null");
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> if <paramref name="value"/> is null or
        /// a <see cref="ArgumentException"/> if <paramref name="value"/> is empty
        /// </summary>
        /// <param name="value">The value to test for null or empty</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstNullOrEmpty(string value, string paramName, string message = null)
        {
            AgainstNull(value, paramName, message);

            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(message ?? $"The string passed for parameter {paramName} was found to be empty", paramName);
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> if <paramref name="value"/> is null or
        /// a <see cref="ArgumentException"/> if <paramref name="value"/> is empty or whites-space
        /// </summary>
        /// <param name="value">The value to test for null, empty or white-space</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstNullOrWhitespace(string value, string paramName, string message = null)
        {
            AgainstNullOrEmpty(value, paramName, message);

            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(message ?? $"The string passed for parameter {paramName} was found to be white-space", paramName);
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if <paramref name="value"/> is the default value of it's type
        /// </summary>
        /// <param name="value">The value to test for default</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        /// <typeparam name="TValue">The type of the value to test for null</typeparam>
        public static void AgainstDefault<TValue>(TValue value, string paramName, string message = null)
            where TValue : struct
        {
            if (value.Equals(default(TValue)))
            {
                throw new ArgumentException(message ?? $"The {typeof(TValue).Name} passed for parameter {paramName} was found to have it's default value", paramName);
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if <paramref name="value"/> is outside the desired inclusive range
        /// </summary>
        /// <param name="value">The value to test for range</param>
        /// <param name="minimumInclusive">The minimum inclusive value</param>
        /// <param name="maximumInclusive">The maximum inclusive value</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstRange(int value, int? minimumInclusive, int? maximumInclusive, string paramName, string message = null)
        {
            if (minimumInclusive == null && maximumInclusive == null)
            {
                throw new ArgumentException($"You must supply a {minimumInclusive} or {maximumInclusive}");
            }

            if (minimumInclusive != null && value < minimumInclusive)
            {
                throw new ArgumentException(message ?? $"The int passed for parameter {paramName} with value {value} must be GTE to {minimumInclusive}");
            }

            if (maximumInclusive != null && value > maximumInclusive)
            {
                throw new ArgumentException(message ?? $"The int passed for parameter {paramName} with value {value} must be LTE {maximumInclusive}");
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if <paramref name="value"/> is outside the desired inclusive range
        /// </summary>
        /// <param name="value">The value to test for range</param>
        /// <param name="minimumInclusive">The minimum inclusive value</param>
        /// <param name="maximumInclusive">The maximum inclusive value</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstRange(double value, double? minimumInclusive, double? maximumInclusive, string paramName, string message = null)
        {
            if (minimumInclusive == null && maximumInclusive == null)
            {
                throw new ArgumentException($"You must supply a {minimumInclusive} or {maximumInclusive}");
            }

            if (minimumInclusive != null && value < minimumInclusive)
            {
                throw new ArgumentException(message ?? $"The double passed for parameter {paramName} with value {value} must be GTE to {minimumInclusive}");
            }

            if (maximumInclusive != null && value > maximumInclusive)
            {
                throw new ArgumentException(message ?? $"The double passed for parameter {paramName} with value {value} must be LTE {maximumInclusive}");
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided Enum value is not valid
        /// </summary>
        /// <typeparam name="TEnum">The Enum Type</typeparam>
        /// <param name="value">The Enum Value</param>
        /// <param name="paramName">The parameter name</param>
        /// <param name="message">The optional message to include in any exception raised</param>
        public static void AgainstInvalidEnum<TEnum>(TEnum value, string paramName, string message = null)
            where TEnum : Enum
        {
            var type = typeof(TEnum);

            // Skip Flags
            if (type.GetCustomAttributes(typeof(FlagsAttribute), true).Length > 0)
            {
                return;
            }

            if (!Enum.IsDefined(type, value))
            {
                throw new ArgumentException(message ?? $"The Enum value passed for parameter {paramName} is not defined on type {type.Name}", paramName);
            }
        }
    }
}