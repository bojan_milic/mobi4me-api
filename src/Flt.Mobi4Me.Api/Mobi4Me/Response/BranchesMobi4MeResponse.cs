﻿using System.Collections.Generic;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class BranchesMobi4MeResponse
    {
        public List<BranchMobi4MeModel> Branches { get; set; }
    }
}
