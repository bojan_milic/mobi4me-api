﻿using Flt.Mobi4Me.Api.Helpers;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class ProfileSummaryMobi4MeResponse
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("isActive")]
        [JsonConverter(typeof(BoolParseStringConverter))]
        public bool IsActive { get; set; }

        [JsonPropertyName("mainAccountID")]
        [JsonConverter(typeof(LongParseStringConverter))]
        public long MainAccountId { get; set; }
        [JsonPropertyName("client")]
        public ClientMobi4MeModel Client { get; set; }

        [JsonPropertyName("personal")]
        public PersonalMobi4MeModel Personal { get; set; }

        [JsonPropertyName("depositAccounts")]
        public List<DepositAccountShortMobi4MeModel> DepositAccounts { get; set; }

        [JsonPropertyName("loanAccounts")]
        public List<LoanAccountShortMobi4MeModel> LoanAccounts { get; set; }

        [JsonPropertyName("accountDropdowns")]
        public List<AccountDropdownMobi4MeModel> AccountDropdowns { get; set; }
    }
}
