﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class AccountTransactionsMobi4MeResponse
    {
        [JsonPropertyName("transactions")]
        public List<TransactionMobi4MeModel> Transactions { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }

        [JsonPropertyName("beforeTransactionID")]
        public string BeforeTransactionId { get; set; }
    }
}
