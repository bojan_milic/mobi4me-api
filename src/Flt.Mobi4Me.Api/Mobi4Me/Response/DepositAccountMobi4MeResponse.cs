﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class DepositAccountMobi4MeResponse
    {
        [JsonPropertyName("account")]
        public DepositAccountMobi4MeModel Account { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }
    }
}
