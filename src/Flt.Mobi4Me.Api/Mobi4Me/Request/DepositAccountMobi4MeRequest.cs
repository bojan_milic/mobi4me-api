﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class DepositAccountMobi4MeRequest : BaseMobi4MeRequest
    {
        [JsonPropertyName("accountID")]
        [DefaultValue("123456")]
        public string AccountId { get; set; }
    }
}
