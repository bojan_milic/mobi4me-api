﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class PotentialClientMobi4MeRequest : BaseMobi4MeRequest
    {
        [DefaultValue("John")]
        [JsonPropertyName("firstName")]
        [Required]
        public string FirstName { get; set; }

        [DefaultValue("Doe")]
        [JsonPropertyName("lastName")]
        [Required]
        public string LastName { get; set; }

        [DefaultValue("Yakutsk")]
        [JsonPropertyName("city")]
        [Required]
        public string City { get; set; }

        [JsonPropertyName("email")]
        [EmailAddress]
        [DefaultValue("some@address.com")]
        public string Email { get; set; }

        [JsonPropertyName("mobilePhone")]
        [Phone]
        [DefaultValue("+381641431242")]
        [Required]
        public string MobilePhone { get; set; }
    }
}
