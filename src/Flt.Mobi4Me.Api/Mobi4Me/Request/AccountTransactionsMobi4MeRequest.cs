﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class AccountTransactionsMobi4MeRequest : BaseMobi4MeRequest
    {
        [JsonPropertyName("accountID")]
        [DefaultValue("123456")]
        public string AccountId { get; set; }

        [JsonPropertyName("beforeTransactionID")]
        [DefaultValue(0)]
        public long BeforeTransactionId { get; set; }
    }
}
