﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class OnBoardMobi4MeRequest : BaseMobi4MeRequest
    {
        [DefaultValue("John")]
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [DefaultValue("Doe")]
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("mobilePhone")]
        [Phone]
        [DefaultValue("+381641431242")]
        [Required]
        public string MobilePhone { get; set; }

        [DefaultValue("1234567890123")]
        [JsonPropertyName("user-identifier")]
        public string UserIdentifier { get; set; }
    }
}
