﻿using Flt.Mobi4Me.Api.Helpers;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class DepositAccountMobi4MeModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("productName")]
        public string ProductName { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("interestRate")]
        [JsonConverter(typeof(DecimalParseStringConverter))]
        public decimal InterestRate { get; set; }

        [JsonPropertyName("balance")]
        public string Balance { get; set; }

        [JsonPropertyName("fixedDeposit")]
        public FixedDepositMobi4MeModel FixedDeposit { get; set; }
    }
}
