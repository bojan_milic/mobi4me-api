﻿using Flt.Mobi4Me.Api.Helpers;
using System;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class TransactionMobi4MeModel
    {
        [JsonPropertyName("id")]
        [JsonConverter(typeof(LongParseStringConverter))]
        public long Id { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("occurredOn")]
        public DateTimeOffset OccurredOn { get; set; }

        [JsonPropertyName("amount")]
        public string Amount { get; set; }

        [JsonPropertyName("balance")]
        public string Balance { get; set; }

        [JsonPropertyName("transactionType")]
        public string TransactionType { get; set; }

        [JsonPropertyName("transactionStatus")]
        public string TransactionStatus { get; set; }

        [JsonPropertyName("isReverted")]
        public string IsReverted { get; set; }

        [JsonPropertyName("referenceNumber")]
        public string ReferenceNumber { get; set; }

        [JsonPropertyName("notes")]
        public string Notes { get; set; }
    }
}
