﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class FixedDepositMobi4MeModel
    {
        [JsonPropertyName("depositAmount")]
        public string DepositAmount { get; set; }

        [JsonPropertyName("expectedInterest")]
        public string ExpectedInterest { get; set; }
    }
}
