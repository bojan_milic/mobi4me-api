﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class ClientMobi4MeModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("externalID")]
        public string ExternalId { get; set; }

        [JsonPropertyName("clientType")]
        public string ClientType { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("branchId")]
        public string BranchId { get; set; }
    }
}
