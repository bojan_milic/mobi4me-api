﻿using Flt.Mobi4Me.Api.Helpers;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Mobi4Me
{
    public class MobileMobi4MeModel
    {
        [JsonPropertyName("regionCode")]
        public string RegionCode { get; set; }

        [JsonPropertyName("number")]
        public string Number { get; set; }

        [JsonPropertyName("isValid")]
        [JsonConverter(typeof(BoolParseStringConverter))]
        public bool IsValid { get; set; }
    }
}
