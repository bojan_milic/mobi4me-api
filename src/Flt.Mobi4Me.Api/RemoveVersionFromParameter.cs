﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace Flt.Mobi4Me.Api
{
    /// <summary>
    /// Remove version from parameter list
    /// </summary>
    public class RemoveVersionFromParameter : IOperationFilter
    {
        /// <summary>
        /// Applies role for removing verison from parameter list
        /// </summary>
        /// <param name="operation">Operation used</param>
        /// <param name="context">Operation filter convext</param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var versionParameter = operation.Parameters.Single(p => p.Name == "version");
            operation.Parameters.Remove(versionParameter);
        }
    }
}
