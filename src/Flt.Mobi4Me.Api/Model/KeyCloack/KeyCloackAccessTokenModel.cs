﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Model
{
    public class KeyCloackAccessTokenModel
    {
        public string Access_Token { get; set; }
        public int Expires_In { get; set; }
    }
}
