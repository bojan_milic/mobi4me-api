﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Model
{
    public class KeyCloakUserModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public bool Enabled { get; set; }
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
