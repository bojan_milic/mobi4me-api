﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Model.KeyCloack
{
    public class Realms
    {
        [JsonPropertyName("mobile")]
        public RoleList Mobile { get; set; }

        [JsonPropertyName("account")]
        public RoleList Account { get; set; }
    }
}
