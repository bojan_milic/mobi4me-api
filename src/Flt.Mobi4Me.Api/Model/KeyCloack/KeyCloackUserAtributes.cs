﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Model
{
    public class KeyCloackUserAtributes
    {
        [JsonPropertyName("client-id")]
        public string ClientId { get; set; }
    }
}
