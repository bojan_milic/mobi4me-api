﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Model
{
    public class KeyCloackCreateUserModel
    {
        public KeyCloackCreateUserModel()
        {
            this.Attributes = new KeyCloackUserAtributes();
            this.Credentials = new List<KeyCloakUserCredentials>();
        }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("enabled")]
        public bool Enabled { get; set; }

        [JsonPropertyName("attributes")]
        public KeyCloackUserAtributes Attributes { get; set; }

        [JsonPropertyName("credentials")]
        public List<KeyCloakUserCredentials> Credentials { get; set; }
    }
}
