﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.Model.KeyCloack
{
    public class RoleList
    {
        [JsonPropertyName("roles")]
        public List<string> Roles { get; set; }
    }
}
