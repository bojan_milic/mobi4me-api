﻿using System.Collections.Generic;

namespace Flt.Mobi4Me.Api.Model
{
    public class MailDefinitionModel
    {
        public List<string> To { get; set; }

        public string Subject { get; set; }

        public string HtmlBody { get; set; }

        public string TextBody { get; set; }

        public MailServerModel MailServer { get; set; }
    }
}
