﻿using Flt.Mobi4Me.Api.Helpers;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class LoanAccountCoreBankModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("productName")]
        public string ProductName { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("loanAmount")]
        public string LoanAmount { get; set; }

        [JsonPropertyName("interestRate")]
        [JsonConverter(typeof(DecimalParseStringConverter))]
        public decimal InterestRate { get; set; }

        [JsonPropertyName("interestInterval")]
        public string InterestInterval { get; set; }

        [JsonPropertyName("numberOfInstalments")]
        [JsonConverter(typeof(LongParseStringConverter))]
        public long NumberOfInstalments { get; set; }

        [JsonPropertyName("principalBalance")]
        public string PrincipalBalance { get; set; }

        [JsonPropertyName("totalOutstanding")]
        public string TotalOutstanding { get; set; }

        [JsonPropertyName("duePrincipal")]
        public string DuePrincipal { get; set; }

        [JsonPropertyName("dueInterest")]
        public string DueInterest { get; set; }

        [JsonPropertyName("dueFees")]
        public string DueFees { get; set; }

        [JsonPropertyName("duePenalties")]
        public string DuePenalties { get; set; }

        [JsonPropertyName("nextInstalmentDate")]
        public string NextInstalmentDate { get; set; }

        [JsonPropertyName("nextInstalmentAmount")]
        public string NextInstalmentAmount { get; set; }

        [JsonPropertyName("totalDue")]
        public string TotalDue { get; set; }
    }
}
