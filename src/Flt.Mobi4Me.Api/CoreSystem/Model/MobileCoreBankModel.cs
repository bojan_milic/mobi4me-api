﻿using Flt.Mobi4Me.Api.Helpers;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class MobileCoreBankModel
    {
        [JsonPropertyName("regionCode")]
        public string RegionCode { get; set; }

        [JsonPropertyName("number")]
        public string Number { get; set; }

        [JsonConverter(typeof(BoolParseStringConverter))]
        [JsonPropertyName("isValid")]
        public bool IsValid { get; set; }
    }
}
