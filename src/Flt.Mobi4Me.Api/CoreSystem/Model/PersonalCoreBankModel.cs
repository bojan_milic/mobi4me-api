﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class PersonalCoreBankModel
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("middleName")]
        public string MiddleName { get; set; }

        [JsonPropertyName("nickname")]
        public string Nickname { get; set; }

        [JsonPropertyName("fathersName")]
        public string FathersName { get; set; }

        [JsonPropertyName("motherMaidenName")]
        public string MotherMaidenName { get; set; }

        [JsonPropertyName("dateOfBirth")]
        public string DateOfBirth { get; set; }

        [JsonPropertyName("placeOfBirth")]
        public string PlaceOfBirth { get; set; }

        [JsonPropertyName("address")]
        public AddressCoreBankModel Address { get; set; }

        [JsonPropertyName("mobile")]
        public MobileCoreBankModel Mobile { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("facebookProfileLink")]
        public string FacebookProfileLink { get; set; }
    }
}
