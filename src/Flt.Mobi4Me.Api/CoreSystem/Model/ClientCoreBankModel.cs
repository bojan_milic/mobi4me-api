﻿using Flt.Mobi4Me.Api.Helpers;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class ClientCoreBankModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("externalId")]
        public string ExternalId { get; set; }

        [JsonPropertyName("clientType")]
        public string ClientType { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("smsNotificationsStatus")]
        [JsonConverter(typeof(BoolParseStringConverter))]
        public bool SmsNotificationsStatus { get; set; }

        [JsonPropertyName("mainAccountId")]
        public string MainAccountId { get; set; }

        [JsonPropertyName("branch")]
        public BranchShortCoreBankModel Branch { get; set; }

        [JsonPropertyName("personal")]
        public PersonalCoreBankModel Personal { get; set; }

        [JsonPropertyName("isActive")]
        [JsonConverter(typeof(BoolParseStringConverter))]
        public bool IsActive { get; set; }
    }
}
