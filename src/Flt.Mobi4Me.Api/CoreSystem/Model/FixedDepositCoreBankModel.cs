﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class FixedDepositCoreBankModel
    {
        [JsonPropertyName("depositAmount")]
        public string DepositAmount { get; set; }

        [JsonPropertyName("expectedInterest")]
        public string ExpectedInterest { get; set; }
    }
}
