﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class LoanAccountCoreBankRequest
    {
        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }
    }
}
