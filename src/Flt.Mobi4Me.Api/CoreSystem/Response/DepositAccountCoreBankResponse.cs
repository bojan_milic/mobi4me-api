﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class DepositAccountCoreBankResponse : BaseCoreBankResponse
    {
        [JsonPropertyName("account")]
        public DepositAccountCoreBankModel Account { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }
    }
}
