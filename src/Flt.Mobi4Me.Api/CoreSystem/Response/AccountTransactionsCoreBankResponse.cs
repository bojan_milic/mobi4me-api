﻿using Flt.Mobi4Me.Api.Helpers;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class AccountTransactionsCoreBankResponse : BaseCoreBankResponse
    {
        [JsonPropertyName("transactions")]
        public List<TransactionCoreBankModel> Transactions { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }

        [JsonPropertyName("beforeTransactionID")]
        [JsonConverter(typeof(LongParseStringConverter))]
        public long BeforeTransactionId { get; set; }
    }
}
