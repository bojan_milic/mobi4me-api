﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class ClientCoreBankResponse
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("client")]
        public ClientCoreBankModel Client { get; set; }

        [JsonPropertyName("deposits")]
        public List<DepositAccountCoreBankModel> Deposits { get; set; }

        [JsonPropertyName("loans")]
        public List<LoanAccountCoreBankModel> Loans { get; set; }

        [JsonPropertyName("totalDepositBalance")]
        public string TotalDepositBalance { get; set; }

        [JsonPropertyName("totalOutstandingLoanBalance")]
        public string TotalOutstandingLoanBalance { get; set; }

        [JsonPropertyName("totalDue")]
        public string TotalDue { get; set; }
    }
}
