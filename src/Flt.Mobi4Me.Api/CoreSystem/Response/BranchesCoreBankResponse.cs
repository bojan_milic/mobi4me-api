﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class BranchesCoreBankResponse : BaseCoreBankResponse
    {
        [JsonPropertyName("branches")]
        public List<BranchCoreBankModel> Branches { get; set; }
    }
}
