﻿using System.Text.Json.Serialization;

namespace Flt.Mobi4Me.Api.CoreSystem
{
    public class LoanAccountCoreBankResponse : BaseCoreBankResponse
    {
        [JsonPropertyName("account")]
        public LoanAccountCoreBankModel Account { get; set; }

        [JsonPropertyName("accountID")]
        public string AccountId { get; set; }

        [JsonPropertyName("clientID")]
        public string ClientId { get; set; }
    }
}
