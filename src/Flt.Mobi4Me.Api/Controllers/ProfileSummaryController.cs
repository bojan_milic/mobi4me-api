﻿using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.ProfileSummary;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/profile-summary")]
    [ApiController]
    public class ProfileSummaryController : BaseApiController
    {
        private readonly IProfileSummary _profileSummary;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="profileSummary">IProfileSummary element</param>
        public ProfileSummaryController(IProfileSummary profileSummary)
        {
            Guard.AgainstNull(profileSummary, nameof(profileSummary));
            _profileSummary = profileSummary;
        }

        /// <summary>
        /// Get full profile summary
        /// </summary>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Profile summary response</response>
        /// <response code="500">if system fails</response>
        [HttpGet]
        public async Task<IActionResult> IndexAsync([FromHeader(Name = "phone-imei")] string phoneImei)
        {
            try
            {
                return Ok(await _profileSummary.GetAsync(phoneImei));
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Save profile settings
        /// </summary>
        /// <param name="request">ProfileSummaryUpdateMobi4MeRequest request</param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Profile summary updated</response>
        /// <response code="500">if system fails</response>
        [HttpPost]
        public async Task<IActionResult> UpdateProfileAsync(ProfileSummaryUpdateMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            try
            {
                return Ok(await _profileSummary.UpdateAsync(request, phoneImei));
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}