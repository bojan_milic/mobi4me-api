﻿using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.DepositAccount;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/deposit-account")]
    [ApiController]
    public class DepositAccountController : BaseApiController
    {
        private readonly IDepositAccount _depositAccount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="depositAccount">IDepositAccount element</param>
        public DepositAccountController(IDepositAccount depositAccount)
        {
            Guard.AgainstNull(depositAccount, nameof(DepositAccount));
            _depositAccount = depositAccount;
        }

        [HttpPost]
        public async Task<IActionResult> GetAccountAsync(DepositAccountMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            return Ok(await _depositAccount.GetAsync(request, phoneImei));
        }
    }
}