﻿using Flt.Mobi4Me.Api.Services.Branches;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/branches")]
    [ApiController]
    [AllowAnonymous]
    public class BranchesController : BaseApiController
    {
        private readonly IBranches _branches;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="branches">IBranches element</param>
        public BranchesController(IBranches branches)
        {
            Guard.AgainstNull(branches, nameof(branches));
            _branches = branches;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync([FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _branches.GetAsync(phoneImei);
            if (response != null)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}