﻿using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.LoanAccount;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/loan-account")]
    [ApiController]
    public class LoanAccountController : BaseApiController
    {
        private readonly ILoanAccount _loanAccount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="loanAccount">ILoanAccount element</param>
        public LoanAccountController(ILoanAccount loanAccount)
        {
            Guard.AgainstNull(loanAccount, nameof(LoanAccount));
            _loanAccount = loanAccount;
        }

        [HttpPost]
        public async Task<IActionResult> GetAccountAsync(LoanAccountMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            return Ok(await _loanAccount.GetAsync(request, phoneImei));
        }
    }
}