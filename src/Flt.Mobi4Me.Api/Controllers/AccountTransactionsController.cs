﻿using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.DepositAccount;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    /// <summary>
    /// Controller for getting account transactions for both types of Accounts (Loan and Deposit)
    /// </summary>
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/account-transactions")]
    [ApiController]
    public class AccountTransactionsController : BaseApiController
    {
        private readonly IAccountTransactions _accountTransactions;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="accountTransactions">IAccountTransactions element</param>
        public AccountTransactionsController(IAccountTransactions accountTransactions)
        {
            Guard.AgainstNull(accountTransactions, nameof(AccountTransactions));
            _accountTransactions = accountTransactions;
        }

        [HttpPost]
        public async Task<IActionResult> GetTransactionsAsync(AccountTransactionsMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            return Ok(await _accountTransactions.GetAsync(request, phoneImei));
        }
    }
}