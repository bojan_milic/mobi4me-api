﻿using Flt.Mobi4Me.Api.ApiException;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flt.Mobi4Me.Api.Controllers
{
    /// <summary>
    /// Base Api Controller
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ApiExceptionFilter]
    public class BaseApiController : ControllerBase
    {
    }
}