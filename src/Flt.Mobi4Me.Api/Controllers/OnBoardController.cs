﻿using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.OnBoard;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [ApiController]
    [AllowAnonymous]
    public class OnBoardController : BaseApiController
    {
        private readonly IOnBoard _onBoard;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="onBoard">IOnBoard element</param>
        public OnBoardController(IOnBoard onBoard)
        {
            Guard.AgainstNull(onBoard, nameof(onBoard));
            _onBoard = onBoard;
        }

        /// <summary>
        /// Sends a request for onboarding clients that already exists on Core banking system
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/on-board
        ///     {
        ///       "firstName": "John",
        ///       "lastName": "Doe",
        ///       "mobilePhone": "+381649999999"
        ///       "user-identifier": "1234567890123"
        ///     }
        ///
        /// </remarks>
        /// <param name="request">Request body</param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Generated token</response>
        /// <response code="412">if phone number already in use</response>
        /// <response code="500">if system fails to send SMS</response>
        [HttpPost]
        [Route("/api/v{version:apiVersion}/on-board/initialize")]
        public async Task<IActionResult> SendRequestAsync(OnBoardMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _onBoard.SendRequestAsync(request, phoneImei);
            if (response.Status == Mobi4MeResponseCode.ClientExist)
            {
                return StatusCode(StatusCodes.Status412PreconditionFailed, $"Phone number {request.MobilePhone} is already in use!");
            }

            if (response.Status == Mobi4MeResponseCode.UnsendSMS)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Failed to send SMS to {request.MobilePhone}.");
            }

            return Ok(response.Token);
        }

        /// <summary>
        /// Validate existing bank client onboarding request with token and digit code
        /// </summary>
        /// <param name="token">Token received by calling post initialize method </param>
        /// <param name="digitCode">Digit code received by SMS</param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Generated temporary password</response>
        /// <response code="404">Token or digit code is invalid and appropriate text is included in response</response>
        /// <response code="408">if there is timeout between sent SMS and validation process (by default it is 300 seconds)</response>
        [HttpPatch]
        [Route("/api/v{version:apiVersion}/on-board/validate")]
        public async Task<IActionResult> ValidateRequestAsync(string token, string digitCode, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _onBoard.ValidateRequestAsync(token, digitCode, phoneImei);
            if (response.Status == Mobi4MeResponseCode.InvalidToken)
            {
                return NotFound($"Token doesn't exists.");
            }

            if (response.Status == Mobi4MeResponseCode.InvalidDigitCode)
            {
                return NotFound($"Invalid digit code.");
            }

            if (response.Status == Mobi4MeResponseCode.RequestTimeout)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout, "Request timeout.");
            }

            if (response.Status == Mobi4MeResponseCode.AlreadyConfirmed)
            {
                return BadRequest("Already confirmed.");
            }

            if (response.Status == Mobi4MeResponseCode.InvalidMobilePhone)
            {
                return NotFound("Invalid mobile phone.");
            }

            if (response.Status == Mobi4MeResponseCode.InternalServerError)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Keycloak user was not created.");
            }

            return StatusCode(StatusCodes.Status201Created, new { username = response.UserName, password = response.TemporaryPassword });
        }

        /// <summary>
        /// Send SMS again with appropriate token
        /// </summary>
        /// <param name="token">Token reveived by calling post method </param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">SMS sent</response>
        /// <response code="404">Token is invalid</response>
        /// <response code="429">Too many requests for this token.</response>
        /// <response code="500">if system fails to send SMS</response>
        [HttpPatch]
        [Route("/api/v{version:apiVersion}/on-board/send-sms-again")]
        public async Task<IActionResult> SendSMSAgainAsync(string token, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _onBoard.SendSMSAgainAsync(token, phoneImei);
            if (response == Mobi4MeResponseCode.TooManyRequests)
            {
                return StatusCode(StatusCodes.Status429TooManyRequests, "Too many requests for this token.");
            }

            if (response == Mobi4MeResponseCode.InvalidToken)
            {
                return NotFound($"Invalid digit code.");
            }

            if (response == Mobi4MeResponseCode.UnsendSMS)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to send SMS to client phone.");
            }

            if (response == Mobi4MeResponseCode.AlreadyConfirmed)
            {
                return BadRequest("Already confirmed.");
            }

            return Ok();
        }

        /// <summary>
        /// Finalize user will be called from Keycloak
        /// </summary>
        /// <param name="userId">Token reveived by calling post method </param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">User confirmed</response>
        /// <response code="429">Too many requests for this token.</response>
        /// <response code="500">if system fails to send SMS</response>
        [HttpPatch]
        [Route("/api/v{version:apiVersion}/on-board/finalize")]
        public async Task<IActionResult> FinalizeAsync(int userId, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _onBoard.FinalizeAsync(userId, phoneImei);
            if (response == Mobi4MeResponseCode.TooManyRequests)
            {
                return StatusCode(StatusCodes.Status429TooManyRequests, "Too many requests for this token.");
            }

            if (response == Mobi4MeResponseCode.AlreadyConfirmed)
            {
                return BadRequest("Already confirmed.");
            }

            return Ok();
        }

        /// <summary>
        /// Delete contact data
        /// </summary>
        /// <param name="phoneNumber">phoneNumber request</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Profile deleted</response>
        /// <response code="403">If user is not with appropriate role</response>
        /// <response code="500">if system fails</response>
        [HttpDelete]
        [Route("/api/v{version:apiVersion}/on-board/delete")]
        public async Task<IActionResult> DeleteProfileAsync(string phoneNumber)
        {
            try
            {
                return Ok(await _onBoard.DeleteAsync(phoneNumber));
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}