﻿using Flt.Mobi4Me.Api.Services.Health;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Flt.Mobi4Me.Api.Controllers
{
    /// <summary>
    /// Place for checking API dependencies status.
    /// </summary>
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/health")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : BaseApiController
    {
        private readonly IHealth _health;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="health">IHealth element</param>
        public HealthController(IHealth health)
        {
            Guard.AgainstNull(health, nameof(health));

            _health = health;
        }

        [HttpGet]
        public string Index()
        {
            return _health.Status();
        }

        [HttpPost]
        public object Throw()
        {
            throw new InvalidOperationException("This is an unhandled exception");
        }
    }
}