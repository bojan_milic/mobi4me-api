﻿using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Services.ProfileSummary;
using Flt.Mobi4Me.Guards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Controllers
{
    [ApiVersion("1")]
    [ApiController]
    [AllowAnonymous]
    public class PotentialClientController : BaseApiController
    {
        private readonly IPotentialClient _potentialClient;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="potentialClient">IPotentialClient element</param>
        public PotentialClientController(IPotentialClient potentialClient)
        {
            Guard.AgainstNull(potentialClient, nameof(potentialClient));
            _potentialClient = potentialClient;
        }

        /// <summary>
        /// Sends a request for potential client
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/potential-client
        ///     {
        ///       "firstName": "John",
        ///       "lastName": "Doe",
        ///       "city": "Yakutsk",
        ///       "email": "some@address.com",
        ///       "mobilePhone": "+381649999999"
        ///     }
        ///
        /// </remarks>
        /// <param name="request">Request body</param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Generated token</response>
        /// <response code="412">if phone number already in use</response>
        /// <response code="500">if system fails to send SMS</response>
        [HttpPost]
        [Route("/api/v{version:apiVersion}/potential-client/initialize")]
        public async Task<IActionResult> SendRequestAsync(PotentialClientMobi4MeRequest request, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _potentialClient.SendRequestAsync(request, phoneImei);
            if (response.Status == Mobi4MeResponseCode.ClientExist)
            {
                return StatusCode(StatusCodes.Status412PreconditionFailed, $"Phone number {request.MobilePhone} is already in use!");
            }

            if (response.Status == Mobi4MeResponseCode.UnsendSMS)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Failed to send SMS to {request.MobilePhone}.");
            }

            return Ok(response.Token);
        }

        /// <summary>
        /// Validate potential client request with token and digit code
        /// </summary>
        /// <param name="token">Token reveived by calling post method </param>
        /// <param name="digitCode">Digit code received by SMS</param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Validated successfully</response>
        /// <response code="400">Potential client already confirmed</response>
        /// <response code="404">Token or digit code is invalid and appropriate text is included in response</response>
        /// <response code="408">if there is timeout between sent SMS and validation process (by default it is 300 seconds)</response>
        [HttpPatch]
        [Route("/api/v{version:apiVersion}/potential-client/validate")]
        public async Task<IActionResult> ValidateRequestAsync(string token, string digitCode, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _potentialClient.ValidateRequestAsync(token, digitCode, phoneImei);
            if (response == Mobi4MeResponseCode.InvalidToken)
            {
                return NotFound($"Token doesn't exists.");
            }

            if (response == Mobi4MeResponseCode.InvalidDigitCode)
            {
                return NotFound($"Invalid digit code.");
            }

            if (response == Mobi4MeResponseCode.RequestTimeout)
            {
                return StatusCode(StatusCodes.Status408RequestTimeout, "Request timeout.");
            }

            if (response == Mobi4MeResponseCode.AlreadyConfirmed)
            {
                return BadRequest("Already confirmed.");
            }

            return Ok();
        }

        /// <summary>
        /// Send SMS again with appropriate token
        /// </summary>
        /// <param name="token">Token reveived by calling post method </param>
        /// <param name="phoneImei">Phone Imei sent over request header</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">SMS sent</response>
        /// <response code="400">Potential client already confirmed</response>
        /// <response code="404">Token is invalid</response>
        /// <response code="429">Too many requests for this token.</response>
        /// <response code="500">if system fails to send SMS</response>
        [HttpPatch]
        [Route("/api/v{version:apiVersion}/potential-client/send-sms-again")]
        public async Task<IActionResult> SendSMSAgainAsync(string token, [FromHeader(Name = "phone-imei")] string phoneImei)
        {
            var response = await _potentialClient.SendSMSAgainAsync(token, phoneImei);
            if (response == Mobi4MeResponseCode.TooManyRequests)
            {
                return StatusCode(StatusCodes.Status429TooManyRequests, "Too many requests for this token.");
            }

            if (response == Mobi4MeResponseCode.InvalidToken)
            {
                return NotFound($"Invalid digit code.");
            }

            if (response == Mobi4MeResponseCode.UnsendSMS)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to send SMS to client phone.");
            }

            if (response == Mobi4MeResponseCode.AlreadyConfirmed)
            {
                return BadRequest("Already confirmed.");
            }

            return Ok();
        }

        /// <summary>
        /// Delete contact data
        /// </summary>
        /// <param name="phoneNumber">phoneNumber request</param>
        /// <returns>
        /// Returns token
        /// </returns>
        /// <response code="200">Profile deleted</response>
        /// <response code="403">If user is not with appropriate role</response>
        /// <response code="500">if system fails</response>
        [HttpDelete]
        [Route("/api/v{version:apiVersion}/potential-client/delete")]
        public async Task<IActionResult> DeleteProfileAsync(string phoneNumber)
        {
            try
            {
                return Ok(await _potentialClient.DeleteAsync(phoneNumber));
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}