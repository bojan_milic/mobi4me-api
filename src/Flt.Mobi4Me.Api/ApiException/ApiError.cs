﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Flt.Mobi4Me.Api.ApiException
{
    public class ApiError
    {
        public ApiError(string message)
        {
            Message = message;
            IsError = true;
        }

        public ApiError(ModelStateDictionary modelState)
        {
            IsError = true;
            if (modelState != null && modelState.Any(m => m.Value.Errors.Count > 0))
            {
                Message = "Please correct the specified errors and try again.";
                Errors = modelState.SelectMany(m => m.Value.Errors);
            }
        }

        public string Message { get; set; }
        public bool IsError { get; set; }
        public string Detail { get; set; }
        public IEnumerable<ModelError> Errors { get; set; }
    }
}
