﻿using System;

namespace Flt.Mobi4Me.Api.ApiException
{
    [Serializable]
    public class ApiException : Exception
    {
        public ApiException()
        {
        }

        public ApiException(string message, int statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        protected ApiException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
        {
            throw new NotImplementedException();
        }

        public int StatusCode { get; set; }
    }
}
