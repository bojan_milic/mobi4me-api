﻿using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.ProfileSummary
{
    public interface IPotentialClient
    {
        Task<dynamic> SendRequestAsync(PotentialClientMobi4MeRequest request, string phoneImei);

        Task<Mobi4MeResponseCode> ValidateRequestAsync(string token, string digitCode, string phoneImei);

        Task<Mobi4MeResponseCode> SendSMSAgainAsync(string token, string phoneImei);

        Task<bool> DeleteAsync(string phoneNumber);
    }
}