﻿using Dapper;
using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Model;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Flt.Mobi4Me.Api.Services.ProfileSummary
{
    /// <summary>
    /// Represents an implementation of IProfileSummary
    /// </summary>
    public class PotentialClient : BaseService, IPotentialClient
    {
        private readonly IOptions<MailDefinitionModel> _appSettings;
        private readonly IConfiguration _configuration;

        public PotentialClient(IOptions<MailDefinitionModel> appSettings, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _appSettings = appSettings;
            _configuration = configuration;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="request">PotentialClientMobi4MeRequest parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<dynamic> SendRequestAsync(PotentialClientMobi4MeRequest request, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            var validationKey = GenerateValidationKey(request.MobilePhone);

            using (var connection = new SqlConnection(connStr))
            {
                var existingRequestCount = await connection.QueryFirstAsync<int>("select count(*) from PotentialClients where ValidationKey = @ValidationKey and Deleted = 0", new { ValidationKey = validationKey });
                if (existingRequestCount > 0)
                {
                    return new { Status = Mobi4MeResponseCode.ClientExist };
                }
            }

            string query = @"INSERT INTO PotentialClients([Guid] ,[DigitCode] ,[ValidationKey] ,[FirstName], [LastName] ,[City] ,[Email] ,[MobilePhone] ,[IMEI] ,[CreatedOn] ,[ModifiedOn] ,[Confirmed] ,[Attempts]) 
                            VALUES (@guid, @DigitCode, @ValidationKey, @FirstName, @LastName, @City, @Email, @MobilePhone, @IMEI, GETDATE(), GETDATE(), 0, 1)";

            var guid = Guid.NewGuid();
            var digitCode = Create4DigitCode();
            if (await SendSMSAsync(request.MobilePhone, digitCode))
            {
                using (var connection = new SqlConnection(connStr))
                {
                    connection.Execute(query, new { Guid = guid.ToString(), FirstName = request.FirstName, LastName = request.LastName, City = request.City, Email = request.Email, MobilePhone = request.MobilePhone, IMEI = phoneImei, DigitCode = digitCode, ValidationKey = validationKey });
                }

                return new { Status = Mobi4MeResponseCode.Successfull, Token = guid.ToString() };
            }
            else
            {
                return new { Status = Mobi4MeResponseCode.UnsendSMS };
            }
        }

        /// <summary>
        /// Validate request
        /// </summary>
        /// <param name="token">token parameter</param>
        /// <param name="digitCode">digitcode parameter</param>
        /// <param name="phoneImei">phoneimei parameter</param>
        /// <returns>Mobi4MeResponseCode</returns>
        public async Task<Mobi4MeResponseCode> ValidateRequestAsync(string token, string digitCode, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            using (var connection = new SqlConnection(connStr))
            {
                var request = await connection.QueryFirstOrDefaultAsync("select GETDATE() as CurrentTime, ModifiedOn, FirstName, LastName, MobilePhone, City, Email, DigitCode, Confirmed from PotentialClients where Guid = @Guid", new { Guid = token });
                if (request == null)
                {
                    return Mobi4MeResponseCode.InvalidToken;
                }

                if (request.DigitCode != digitCode)
                {
                    return Mobi4MeResponseCode.InvalidDigitCode;
                }

                if (((DateTime)request.ModifiedOn).AddSeconds(_configuration.GetValue<int>("SMSTimeout")) < ((DateTime)request.CurrentTime))
                {
                    return Mobi4MeResponseCode.RequestTimeout;
                }

                if (request.Confirmed)
                {
                    return Mobi4MeResponseCode.AlreadyConfirmed;
                }
                else
                {
                    MimeMessage message = new MimeMessage();

                    MailboxAddress from = new MailboxAddress($"{request.FirstName} {request.LastName}", request.Email);
                    message.From.Add(from);

                    foreach (var email in _appSettings.Value.To)
                    {
                        MailboxAddress to = new MailboxAddress(email, email);
                        message.To.Add(to);
                    }

                    message.Subject = string.Format(CultureInfo.InvariantCulture, _appSettings.Value.Subject, request.FirstName, request.LastName);
                    BodyBuilder bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = string.Format(CultureInfo.InvariantCulture, _appSettings.Value.HtmlBody, request.FirstName, request.LastName, request.City, request.Email, request.MobilePhone, DateTime.Now.ToShortDateString());
                    bodyBuilder.TextBody = string.Format(CultureInfo.InvariantCulture, _appSettings.Value.TextBody, request.FirstName, request.LastName, request.City, request.Email, request.MobilePhone, DateTime.Now.ToShortDateString());
                    message.Body = bodyBuilder.ToMessageBody();

                    using (var client = new SmtpClient())
                    {
                        await client.ConnectAsync(_appSettings.Value.MailServer.Address, _appSettings.Value.MailServer.Port, _appSettings.Value.MailServer.UseSSL);
                        await client.AuthenticateAsync(_appSettings.Value.MailServer.Username, _appSettings.Value.MailServer.Password);
                        await client.SendAsync(message);
                        await client.DisconnectAsync(true);
                    }

                    connection.Execute("update PotentialClients set Confirmed = 1, ModifiedOn = GETDATE() where Guid = @Guid", new { Guid = token });
                }
            }

            return Mobi4MeResponseCode.Successfull;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="token">token parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>Mobi4MeResponseCode</returns>
        public async Task<Mobi4MeResponseCode> SendSMSAgainAsync(string token, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            using (var connection = new SqlConnection(connStr))
            {
                var request = await connection.QueryFirstOrDefaultAsync("select Confirmed, Attempts, MobilePhone, DigitCode from PotentialClients where Guid = @Guid", new { Guid = token });
                if (request != null)
                {
                    if (request.Confirmed)
                    {
                        return Mobi4MeResponseCode.AlreadyConfirmed;
                    }

                    if (request.Attempts < _configuration.GetValue<int>("SMSMaxAttemptsNumber"))
                    {
                        if (await SendSMSAsync(request.MobilePhone, request.DigitCode))
                        {
                            await connection.ExecuteAsync("UPDATE PotentialClients SET Attempts = Attempts + 1, ModifiedOn = GETDATE() where Guid = @Guid", new { Guid = token });
                        }
                        else
                        {
                            return Mobi4MeResponseCode.UnsendSMS;
                        }
                    }
                    else
                    {
                        return Mobi4MeResponseCode.TooManyRequests;
                    }
                }
                else
                {
                    return Mobi4MeResponseCode.InvalidToken;
                }

                return Mobi4MeResponseCode.Successfull;
            }
        }

        public async Task<bool> DeleteAsync(string phoneNumber)
        {
            if (IsInRole("user_admin"))
            {
                var validationKey = GenerateValidationKey(phoneNumber);
                try
                {
                    string connStr = _configuration.GetValue<string>("ConnectionString");
                    using (var connection = new SqlConnection(connStr))
                    {
                        await connection.QueryAsync("update [dbo].[PotentialClients] set Deleted = 1 where ValidationKey = @validationKey", new { validationKey = validationKey });
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        private string Create4DigitCode()
        {
            Random rdm = new Random();
            return rdm.Next(0000, 9999).ToString("D4", CultureInfo.InvariantCulture);
        }

        private async Task<bool> SendSMSAsync(string mobilePhone, string digitCode)
        {
            TwilioClient.Init(_configuration.GetValue<string>("Twilio:Username"), _configuration.GetValue<string>("Twilio:Password"));
            var result = await MessageResource.CreateAsync(body: $"Code: {digitCode}", from: new Twilio.Types.PhoneNumber(_configuration.GetValue<string>("Twilio:PhoneNumber")), to: new Twilio.Types.PhoneNumber(mobilePhone));
            return result.Status != MessageResource.StatusEnum.Failed;
        }

        private string GenerateValidationKey(string phone)
        {
            var validationKey = phone.Replace(" ", string.Empty);
            if (validationKey.Length > 9)
            {
                validationKey = validationKey.Substring(validationKey.Length - 9);
            }

            return validationKey;
        }
    }
}