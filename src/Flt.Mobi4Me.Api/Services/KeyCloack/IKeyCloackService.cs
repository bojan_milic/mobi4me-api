﻿using Flt.Mobi4Me.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services
{
    public interface IKeyCloackService
    {
        Task<bool> GetProfileByPhoneAsync(string phoneNumber);
        Task<bool> CreateUserAsync(KeyCloackCreateUserModel user);
    }
}
