﻿using Flt.Mobi4Me.Api.Model;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services
{
    public class KeyCloackService : IKeyCloackService
    {
        private const string AcCacheKey = "accessToken";
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _cache;

        public KeyCloackService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _cache = memoryCache;
        }

        public async Task<bool> CreateUserAsync(KeyCloackCreateUserModel user)
        {
            try
            {
                var accessToken = await GetTokenAsync();
                var client = new RestClient(_configuration.GetValue<string>("KeyCloak:UsersUrl"));
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("authorization", "Bearer " + accessToken.Access_Token);
                var requestJson = JsonSerializer.Serialize(user);
                request.AddParameter("Application/Json", requestJson, ParameterType.RequestBody);
                var keyCloakResponse = await client.ExecuteAsync(request);

                if (keyCloakResponse != null && keyCloakResponse.IsSuccessful)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return ex.Message.Length > 0 && false;
            }
        }

        public async Task<bool> GetProfileByPhoneAsync(string phoneNumber)
        {
            try
            {
                var accessToken = await GetTokenAsync();
                var client = new RestClient(_configuration.GetValue<string>("KeyCloak:UsersUrl"));
                var request = new RestRequest(Method.GET);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("authorization", "Bearer " + accessToken.Access_Token);
                request.AddParameter("username", phoneNumber);

                var keyCloakResponse = await client.ExecuteAsync(request);
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                var response = JsonSerializer.Deserialize<IEnumerable<KeyCloakUserModel>>(keyCloakResponse.Content, options);

                if (response != null && response.Any())
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return ex.Message.Length > 0 && false;
            }
        }

        private async Task<KeyCloackAccessTokenModel> GetTokenAsync()
        {
            try
            {
                _cache.TryGetValue(AcCacheKey, out KeyCloackAccessTokenModel response);

                if (response != null)
                {
                    return response;
                }

                var client = new RestClient(_configuration.GetValue<string>("KeyCloak:AuthUrl"));
                var request = new RestRequest(Method.POST);

                var requestModel = new KeyCloackTokenRequestModel()
                {
                    GrantType = "password",
                    Username = _configuration.GetValue<string>("KeyCloak:Username"),
                    Password = _configuration.GetValue<string>("KeyCloak:Password"),
                    ClientId = _configuration.GetValue<string>("KeyCloak:ClientId"),
                };

                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", $"username={requestModel.Username}&password={requestModel.Password}&grant_type={requestModel.GrantType}&client_id={requestModel.ClientId}", ParameterType.RequestBody);
                var keyCloakResponse = await client.ExecuteAsync(request);

                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<KeyCloackAccessTokenModel>(keyCloakResponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(response.Expires_In));
                _cache.Set<KeyCloackAccessTokenModel>(AcCacheKey, response, cacheEntryOptions);

                return response;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
