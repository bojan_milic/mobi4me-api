﻿namespace Flt.Mobi4Me.Api.Services.Health
{
    /// <summary>
    /// Represents a health interface
    /// </summary>
    public interface IHealth
    {
        /// <summary>
        /// Prints the health status
        /// </summary>
        /// <returns>The status</returns>
        string Status();

        /// <summary>
        /// Prints the health status for version 2
        /// </summary>
        /// <returns>The status</returns>
        string Status_v2();
    }
}