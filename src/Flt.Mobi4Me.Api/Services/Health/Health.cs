﻿namespace Flt.Mobi4Me.Api.Services.Health
{
    /// <summary>
    /// Represents an implementation of IHealth
    /// </summary>
    public class Health : IHealth
    {
        /// <summary>
        /// Prints the health status
        /// </summary>
        /// <returns>The status</returns>
        public string Status()
        {
            return "Mobi4Me API Heartbeat";
        }

        /// <summary>
        /// Prints the health status for v2
        /// </summary>
        /// <returns>The status</returns>
        public string Status_v2()
        {
            return "Mobi4Me API Heartbeat v2";
        }
    }
}