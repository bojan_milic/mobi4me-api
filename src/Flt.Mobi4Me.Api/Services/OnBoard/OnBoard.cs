﻿using AutoMapper;
using Dapper;
using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using Flt.Mobi4Me.Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Flt.Mobi4Me.Api.Services.OnBoard
{
    /// <summary>
    /// Represents an implementation of IProfileSummary
    /// </summary>
    public class OnBoard : BaseService, IOnBoard
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly ICoreBankingApiService _coreBankingApiService;
        private readonly IKeyCloackService _keyCloackService;
        public OnBoard(IConfiguration configuration, IMapper mapper, ICoreBankingApiService coreBankingApiService, IKeyCloackService keyCloackService, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _configuration = configuration;
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
            _keyCloackService = keyCloackService;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="request">OnBoardMobi4MeRequest parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<dynamic> SendRequestAsync(OnBoardMobi4MeRequest request, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            var validationKey = GenerateValidationKey(request.MobilePhone);

            // Check is user exists on Keycloack!
            var keyCloackUserExist = await _keyCloackService.GetProfileByPhoneAsync(request.MobilePhone);
            if (keyCloackUserExist)
            {
                return new { Status = Mobi4MeResponseCode.ClientExist };
            }

            using (var connection = new SqlConnection(connStr))
            {
                var existingRequestCount = await connection.QueryFirstAsync<int>("select count(*) from [users].[users] where ValidationKey = @ValidationKey", new { ValidationKey = validationKey });
                if (existingRequestCount > 0)
                {
                    return new { Status = Mobi4MeResponseCode.ClientExist };
                }
            }

            string query = @"INSERT INTO [users].[users]([Guid] ,[DigitCode] ,[ValidationKey] ,[FirstName], [LastName] ,[MobilePhone] ,[IMEI] ,[CreatedOn] ,[ModifiedOn] ,[UserStatus] ,[Attempts]) 
                            VALUES (@guid, @DigitCode, @ValidationKey, @FirstName, @LastName, @MobilePhone, @IMEI, GETDATE(), GETDATE(), 0, 1)";

            var guid = Guid.NewGuid();
            var digitCode = Create4DigitCode();
            if (await SendSMSAsync(request.MobilePhone, digitCode))
            {
                using (var connection = new SqlConnection(connStr))
                {
                    connection.Execute(query, new { Guid = guid.ToString(), FirstName = request.FirstName, LastName = request.LastName, MobilePhone = request.MobilePhone, IMEI = phoneImei, DigitCode = digitCode, ValidationKey = validationKey });
                }

                return new { Status = Mobi4MeResponseCode.Successfull, Token = guid.ToString() };
            }
            else
            {
                return new { Status = Mobi4MeResponseCode.UnsendSMS };
            }
        }

        /// <summary>
        /// Validate request
        /// </summary>
        /// <param name="token">token parameter</param>
        /// <param name="digitCode">digitcode parameter</param>
        /// <param name="phoneImei">phoneimei parameter</param>
        /// <returns>Mobi4MeResponseCode</returns>
        public async Task<dynamic> ValidateRequestAsync(string token, string digitCode, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            using (var connection = new SqlConnection(connStr))
            {
                var request = await connection.QueryFirstOrDefaultAsync("select id, GETDATE() as CurrentTime, ModifiedOn, FirstName, LastName, MobilePhone, DigitCode, UserStatus from [users].[users] where Guid = @Guid", new { Guid = token });
                if (request == null)
                {
                    return new { Status = Mobi4MeResponseCode.InvalidToken };
                }

                if (request.DigitCode != digitCode)
                {
                    return new { Status = Mobi4MeResponseCode.InvalidDigitCode };
                }

                if (((DateTime)request.ModifiedOn).AddSeconds(_configuration.GetValue<int>("SMSTimeout")) < ((DateTime)request.CurrentTime))
                {
                    return new { Status = Mobi4MeResponseCode.RequestTimeout };
                }

                if ((int)request.UserStatus == 0)
                {
                    var temporaryPassword = Create4DigitCode();
                    var coreUserResponse = await _coreBankingApiService.GetProfileSummaryAsync(string.Empty, request.MobilePhone);
                    if (coreUserResponse != null)
                    {
                        ProfileSummaryMobi4MeResponse response = _mapper.Map<ProfileSummaryMobi4MeResponse>(coreUserResponse);

                        // call saving user into keycloack with sending client id from instafin call
                        KeyCloackCreateUserModel createModel = new KeyCloackCreateUserModel()
                        {
                            FirstName = response.Personal.FirstName,
                            LastName = response.Personal.LastName,
                            Email = response.Personal.Email,
                            Enabled = true,
                            Username = request.id.ToString()
                        };

                        createModel.Attributes.ClientId = response.Id.ToString();
                        createModel.Credentials.Add(new KeyCloakUserCredentials()
                        {
                            Type = "password",
                            Value = temporaryPassword,
                            Temporary = false,
                        });

                        var keyCloackUserCreated = await _keyCloackService.CreateUserAsync(createModel);
                        if (keyCloackUserCreated)
                        {
                            await connection.ExecuteAsync("update [users].[users] set UserStatus = 1, ModifiedOn = GETDATE() where Guid = @Guid", new { Guid = token });
                            await connection.ExecuteAsync(@"insert [presentation].[account_dropdown]([UserId], [CreatedOn] ,[ModifiedOn] ,[Type] ,[Label], [Order]) 
                            VALUES(@UserId, GETDATE(), GETDATE(), 'deposit', 'Deposit', 1)", new { UserId = request.id.ToString() });
                            await connection.ExecuteAsync(@"insert [presentation].[account_dropdown]([UserId], [CreatedOn] ,[ModifiedOn] ,[Type] ,[Label], [Order]) 
                            VALUES(@UserId, GETDATE(), GETDATE(), 'loan', 'Loan', 2)", new { UserId = request.id.ToString() });

                            return new { Status = Mobi4MeResponseCode.Successfull, TemporaryPassword = temporaryPassword, UserName = request.id.ToString() };
                        }

                        return new { Status = Mobi4MeResponseCode.InternalServerError };
                    }
                    else
                    {
                        return new { Status = Mobi4MeResponseCode.InvalidMobilePhone };
                    }
                }
                else
                {
                    return new { Status = Mobi4MeResponseCode.AlreadyConfirmed };
                }
            }
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="token">token parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>Mobi4MeResponseCode</returns>
        public async Task<Mobi4MeResponseCode> SendSMSAgainAsync(string token, string phoneImei)
        {
            string connStr = _configuration.GetValue<string>("ConnectionString");
            using (var connection = new SqlConnection(connStr))
            {
                var request = await connection.QueryFirstOrDefaultAsync("select Attempts, MobilePhone, DigitCode, UserStatus from [users].[users] where Guid = @Guid", new { Guid = token });
                if (request != null)
                {
                    if ((int)request.UserStatus > 0)
                    {
                        return Mobi4MeResponseCode.AlreadyConfirmed;
                    }

                    if (request.Attempts < _configuration.GetValue<int>("SMSMaxAttemptsNumber"))
                    {
                        if (await SendSMSAsync(request.MobilePhone, request.DigitCode))
                        {
                            await connection.ExecuteAsync("UPDATE [users].[users] SET Attempts = Attempts + 1, ModifiedOn = GETDATE() where Guid = @Guid", new { Guid = token });
                        }
                        else
                        {
                            return Mobi4MeResponseCode.UnsendSMS;
                        }
                    }
                    else
                    {
                        return Mobi4MeResponseCode.TooManyRequests;
                    }
                }
                else
                {
                    return Mobi4MeResponseCode.InvalidToken;
                }

                return Mobi4MeResponseCode.Successfull;
            }
        }

        /// <summary>
        /// Finalize user login
        /// </summary>
        /// <param name="userId">user id </param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>success if everything is ok</returns>
        public async Task<Mobi4MeResponseCode> FinalizeAsync(int userId, string phoneImei)
        {
            try
            {
                string connStr = _configuration.GetValue<string>("ConnectionString");
                using (var connection = new SqlConnection(connStr))
                {
                    await connection.ExecuteAsync("update [users].[users] set UserStatus = 2, ModifiedOn = GETDATE() where KeycloakUserId = @userId", new { userId = userId });
                    return Mobi4MeResponseCode.Successfull;
                }
            }
            catch
            {
                return Mobi4MeResponseCode.InternalServerError;
            }
        }

        /// <summary>
        /// Delete onboard users by phone number
        /// </summary>
        /// <param name="phoneNumber">phone number</param>
        /// <returns>true if exists</returns>
        public async Task<bool> DeleteAsync(string phoneNumber)
        {
            if (IsInRole("user_admin"))
            {
                var validationKey = GenerateValidationKey(phoneNumber);
                try
                {
                    string connStr = _configuration.GetValue<string>("ConnectionString");
                    using (var connection = new SqlConnection(connStr))
                    {
                        await connection.QueryAsync("update [users].[users] set Deleted = 1 where ValidationKey = @validationKey", new { validationKey = validationKey });
                        await connection.QueryAsync("update [presentation].[account_dropdown] set Deleted = 1 from [users].[users] where UserId = [users].[users].Id and ValidationKey = @validationKey", new { validationKey = validationKey });

                        // here we need to delete this record in Keycloak
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        private string Create4DigitCode()
        {
            Random rdm = new Random();
            return rdm.Next(0000, 9999).ToString("D4", CultureInfo.InvariantCulture);
        }

        private async Task<bool> SendSMSAsync(string mobilePhone, string digitCode)
        {
            TwilioClient.Init(_configuration.GetValue<string>("Twilio:Username"), _configuration.GetValue<string>("Twilio:Password"));
            var result = await MessageResource.CreateAsync(body: $"Code: {digitCode}", from: new Twilio.Types.PhoneNumber(_configuration.GetValue<string>("Twilio:PhoneNumber")), to: new Twilio.Types.PhoneNumber(mobilePhone));
            return result.Status != MessageResource.StatusEnum.Failed;
        }

        private string GenerateValidationKey(string phone)
        {
            var validationKey = phone.Replace(" ", string.Empty);
            if (validationKey.Length > 9)
            {
                validationKey = validationKey.Substring(validationKey.Length - 9);
            }

            return validationKey;
        }
    }
}