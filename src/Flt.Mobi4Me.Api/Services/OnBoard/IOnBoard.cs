﻿using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.OnBoard
{
    public interface IOnBoard
    {
        Task<dynamic> SendRequestAsync(OnBoardMobi4MeRequest request, string phoneImei);

        Task<dynamic> ValidateRequestAsync(string token, string digitCode, string phoneImei);

        Task<Mobi4MeResponseCode> SendSMSAgainAsync(string token, string phoneImei);

        Task<Mobi4MeResponseCode> FinalizeAsync(int userId, string phoneImei);

        Task<bool> DeleteAsync(string phoneNumber);
    }
}