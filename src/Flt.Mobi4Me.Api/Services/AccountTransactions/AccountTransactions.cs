﻿using AutoMapper;
using Flt.Mobi4Me.Api.Mobi4Me;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.DepositAccount
{
    /// <summary>
    /// Represents an implementation of IAccountTransactions
    /// </summary>
    public class AccountTransactions : BaseService, IAccountTransactions
    {
        private readonly IMapper _mapper;
        private readonly ICoreBankingApiService _coreBankingApiService;

        public AccountTransactions(IMapper mapper, ICoreBankingApiService coreBankingApiService, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="request">AccountTransactionsMobi4MeRequest parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<AccountTransactionsMobi4MeResponse> GetAsync(AccountTransactionsMobi4MeRequest request, string phoneImei)
        {
            var corebankresponse = await _coreBankingApiService.GetAccountTransactionsAsync(request.AccountId, GetClientId(), request.BeforeTransactionId);
            var response = _mapper.Map<AccountTransactionsMobi4MeResponse>(corebankresponse);
            return response;
        }
    }
}