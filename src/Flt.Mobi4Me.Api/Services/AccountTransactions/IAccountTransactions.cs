﻿using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.DepositAccount
{
    public interface IAccountTransactions
    {
        Task<AccountTransactionsMobi4MeResponse> GetAsync(AccountTransactionsMobi4MeRequest request, string phoneImei);
    }
}