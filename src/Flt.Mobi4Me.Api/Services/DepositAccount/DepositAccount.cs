﻿using AutoMapper;
using Flt.Mobi4Me.Api.Mobi4Me;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.DepositAccount
{
    /// <summary>
    /// Represents an implementation of IProfileSummary
    /// </summary>
    public class DepositAccount : BaseService, IDepositAccount
    {
        private readonly IMapper _mapper;
        private readonly ICoreBankingApiService _coreBankingApiService;

        public DepositAccount(IMapper mapper, ICoreBankingApiService coreBankingApiService, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="request">DepositAccountMobi4MeRequest parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<DepositAccountMobi4MeResponse> GetAsync(DepositAccountMobi4MeRequest request, string phoneImei)
        {
            var corebankresponse = await _coreBankingApiService.GetDepositAccountAsync(request.AccountId, GetClientId());
            var response = _mapper.Map<DepositAccountMobi4MeResponse>(corebankresponse);
            return response;
        }
    }
}