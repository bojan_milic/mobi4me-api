﻿using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.DepositAccount
{
    public interface IDepositAccount
    {
        Task<DepositAccountMobi4MeResponse> GetAsync(DepositAccountMobi4MeRequest request, string phoneImei);
    }
}