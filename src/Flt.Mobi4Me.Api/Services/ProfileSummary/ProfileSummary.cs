﻿using AutoMapper;
using Dapper;
using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.ProfileSummary
{
    /// <summary>
    /// Represents an implementation of IProfileSummary
    /// </summary>
    public class ProfileSummary : BaseService, IProfileSummary
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICoreBankingApiService _coreBankingApiService;

        public ProfileSummary(IConfiguration configuration, IMapper mapper, ICoreBankingApiService coreBankingApiService, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _configuration = configuration;
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<ProfileSummaryMobi4MeResponse> GetAsync(string phoneImei)
        {
            var corebankresponse = await _coreBankingApiService.GetProfileSummaryAsync(GetClientId(), string.Empty);
            var response = _mapper.Map<ProfileSummaryMobi4MeResponse>(corebankresponse);
            string connStr = _configuration.GetValue<string>("ConnectionString");
            using (var connection = new SqlConnection(connStr))
            {
                var request = await connection.QueryAsync<AccountDropdownMobi4MeModel>("select [type], [label], [order] from [presentation].[account_dropdown] a inner join [users].[users] u on a.UserId = u.Id where u.Deleted = 0 and a.Deleted = 0 and u.BankUserId = @Guid", new { Guid = GetClientId() });
                response.AccountDropdowns = request.ToList();
            }

            return response;
        }

        public Task<Mobi4MeResponseCode> UpdateAsync(ProfileSummaryUpdateMobi4MeRequest request, string phoneImei)
        {
            // here we need to update user detail
            return Task.FromResult<Mobi4MeResponseCode>(Mobi4MeResponseCode.Successfull);
        }
    }
}