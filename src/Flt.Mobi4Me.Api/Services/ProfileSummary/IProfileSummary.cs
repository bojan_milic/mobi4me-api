﻿using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.ProfileSummary
{
    public interface IProfileSummary
    {
        Task<ProfileSummaryMobi4MeResponse> GetAsync(string phoneImei);
        Task<Mobi4MeResponseCode> UpdateAsync(ProfileSummaryUpdateMobi4MeRequest request, string phoneImei);
    }
}