﻿using Flt.Mobi4Me.Api.CoreSystem;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Globalization;
using System.Text.Json;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services
{
    public class CoreBankingApiService : ICoreBankingApiService
    {
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _cache;
        private IRestClient _client;

        public CoreBankingApiService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _cache = memoryCache;
        }

        public async Task<BranchesCoreBankResponse> GetBranchesAsync()
        {
            var cacheKey = $"branches";
            _cache.TryGetValue(cacheKey, out BranchesCoreBankResponse response);

            if (response != null)
            {
                return response;
            }

            var client = GetClient();
            var request = new RestRequest("mobile.ListBranches", Method.POST);
            request.AddParameter("Application/Json", "{}", ParameterType.RequestBody);
            var corebankresponse = await client.ExecutePostAsync(request);
            if (corebankresponse.IsSuccessful)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<BranchesCoreBankResponse>(corebankresponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_configuration.GetValue<int>("CoreBankingApi:CacheDurationInSeconds")));
                _cache.Set<BranchesCoreBankResponse>(cacheKey, response, cacheEntryOptions);

                return response;
            }
            else
            {
                ThrowErrorIfNeeded(corebankresponse);
                return null;
            }
        }

        public async Task<ClientCoreBankResponse> GetProfileSummaryAsync(string clientId, string phoneNumber)
        {
            var cacheKey = $"profile-summary {clientId} {phoneNumber}";
            _cache.TryGetValue(cacheKey, out ClientCoreBankResponse response);

            if (response != null)
            {
                return response;
            }

            var client = GetClient();
            var request = new RestRequest("mobile.LookupClient", Method.POST);
            var requestJson = JsonSerializer.Serialize(new ClientCoreBankRequest() { Id = clientId });
            request.AddParameter("Application/Json", requestJson, ParameterType.RequestBody);
            var corebankresponse = await client.ExecutePostAsync(request);

            if (corebankresponse.IsSuccessful)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<ClientCoreBankResponse>(corebankresponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_configuration.GetValue<int>("CoreBankingApi:CacheDurationInSeconds")));
                _cache.Set<ClientCoreBankResponse>(cacheKey, response, cacheEntryOptions);

                return response;
            }
            else
            {
                ThrowErrorIfNeeded(corebankresponse);
                return null;
            }
        }

        public async Task<DepositAccountCoreBankResponse> GetDepositAccountAsync(string accountId, string clientId)
        {
            var cacheKey = $"deposit-account {accountId} {clientId}";
            _cache.TryGetValue(cacheKey, out DepositAccountCoreBankResponse response);

            if (response != null)
            {
                return response;
            }

            var client = GetClient();
            var request = new RestRequest("mobile.LookupDepositAccount", Method.POST);
            var requestJson = JsonSerializer.Serialize(new DepositAccountCoreBankRequest() { AccountId = accountId, ClientId = clientId });
            request.AddParameter("Application/Json", requestJson, ParameterType.RequestBody);
            var corebankresponse = await client.ExecutePostAsync(request);

            if (corebankresponse.IsSuccessful)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<DepositAccountCoreBankResponse>(corebankresponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_configuration.GetValue<int>("CoreBankingApi:CacheDurationInSeconds")));
                _cache.Set<DepositAccountCoreBankResponse>(cacheKey, response, cacheEntryOptions);
            }
            else
            {
                ThrowErrorIfNeeded(corebankresponse);
                return null;
            }

            return response;
        }

        public async Task<LoanAccountCoreBankResponse> GetLoanAccountAsync(string accountId, string clientId)
        {
            var cacheKey = $"loan-account {accountId} {clientId}";
            _cache.TryGetValue(cacheKey, out LoanAccountCoreBankResponse response);

            if (response != null)
            {
                return response;
            }

            var client = GetClient();
            var request = new RestRequest("mobile.LookupLoanAccount", Method.POST);
            var requestJson = JsonSerializer.Serialize(new LoanAccountCoreBankRequest() { AccountId = accountId, ClientId = clientId });
            request.AddParameter("Application/Json", requestJson, ParameterType.RequestBody);
            var corebankresponse = await client.ExecutePostAsync(request);
            if (corebankresponse.IsSuccessful)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<LoanAccountCoreBankResponse>(corebankresponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_configuration.GetValue<int>("CoreBankingApi:CacheDurationInSeconds")));
                _cache.Set<LoanAccountCoreBankResponse>(cacheKey, response, cacheEntryOptions);

                return response;
            }
            else
            {
                ThrowErrorIfNeeded(corebankresponse);
                return null;
            }
        }

        public async Task<AccountTransactionsCoreBankResponse> GetAccountTransactionsAsync(string accountId, string clientId, long beforeTransactionId)
        {
            var cacheKey = $"account-transactions {accountId} {clientId} {beforeTransactionId}";
            _cache.TryGetValue(cacheKey, out AccountTransactionsCoreBankResponse response);

            if (response != null)
            {
                return response;
            }

            var client = GetClient();
            var request = new RestRequest("mobile.LookupAccountTransactions", Method.POST);
            var requestJson = JsonSerializer.Serialize(new AccountTransactionsCoreBankRequest() { AccountId = accountId, ClientId = clientId, BeforeTransactionId = beforeTransactionId.ToString(CultureInfo.InvariantCulture) });
            request.AddParameter("Application/Json", requestJson, ParameterType.RequestBody);
            var corebankresponse = await client.ExecutePostAsync(request);

            if (corebankresponse.IsSuccessful)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                response = JsonSerializer.Deserialize<AccountTransactionsCoreBankResponse>(corebankresponse.Content, options);

                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromSeconds(_configuration.GetValue<int>("CoreBankingApi:CacheDurationInSeconds")));
                _cache.Set<AccountTransactionsCoreBankResponse>(cacheKey, response, cacheEntryOptions);

                return response;
            }
            else
            {
                ThrowErrorIfNeeded(corebankresponse);
                return null;
            }
        }

        private void ThrowErrorIfNeeded(IRestResponse response)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                throw new AccessViolationException();
            }

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                var res = JsonSerializer.Deserialize<BaseCoreBankResponse>(response.Content, options);
                if (res != null && !string.IsNullOrWhiteSpace(res.Message))
                {
                    throw new ApiException.ApiException($"Error for field {res.FieldRef}: {res.Message}", 500);
                }

                throw new ApiException.ApiException();
            }
        }

        private IRestClient GetClient()
        {
            if (_client == null)
            {
                var client = new RestClient(_configuration.GetValue<string>("CoreBankingApi:Url"));
                client.Authenticator = new HttpBasicAuthenticator(_configuration.GetValue<string>("CoreBankingApi:Username"), _configuration.GetValue<string>("CoreBankingApi:Password"));
                _client = client;
            }

            return _client;
        }
    }
}
