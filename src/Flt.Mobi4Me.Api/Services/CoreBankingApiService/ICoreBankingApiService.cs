﻿using Flt.Mobi4Me.Api.CoreSystem;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services
{
    public interface ICoreBankingApiService
    {
        Task<ClientCoreBankResponse> GetProfileSummaryAsync(string clientId, string phoneNumber);
        Task<BranchesCoreBankResponse> GetBranchesAsync();
        Task<DepositAccountCoreBankResponse> GetDepositAccountAsync(string accountId, string clientId);
        Task<LoanAccountCoreBankResponse> GetLoanAccountAsync(string accountId, string clientId);
        Task<AccountTransactionsCoreBankResponse> GetAccountTransactionsAsync(string accountId, string clientId, long beforeTransactionId);
    }
}