﻿using AutoMapper;
using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.Branches
{
    /// <summary>
    /// Represents an implementation of IHealth
    /// </summary>
    public class Branches : IBranches
    {
        private readonly IMapper _mapper;
        private readonly ICoreBankingApiService _coreBankingApiService;

        public Branches(IMapper mapper, ICoreBankingApiService coreBankingApiService)
        {
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
        }

        /// <summary>
        /// Prints the health status
        /// </summary>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<BranchesMobi4MeResponse> GetAsync(string phoneImei)
        {
            var corebankresponse = await _coreBankingApiService.GetBranchesAsync();
            if (corebankresponse != null)
            {
                var response = _mapper.Map<BranchesMobi4MeResponse>(corebankresponse);
                return response;
            }
            else
            {
                return null;
            }
        }
    }
}