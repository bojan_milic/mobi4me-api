﻿using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.Branches
{
    public interface IBranches
    {
        Task<BranchesMobi4MeResponse> GetAsync(string phoneImei);
    }
}