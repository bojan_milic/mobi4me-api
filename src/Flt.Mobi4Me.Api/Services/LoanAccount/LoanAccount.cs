﻿using AutoMapper;
using Flt.Mobi4Me.Api.Mobi4Me;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.LoanAccount
{
    /// <summary>
    /// Represents an implementation of IProfileSummary
    /// </summary>
    public class LoanAccount : BaseService, ILoanAccount
    {
        private readonly IMapper _mapper;
        private readonly ICoreBankingApiService _coreBankingApiService;

        public LoanAccount(IMapper mapper, ICoreBankingApiService coreBankingApiService, IHttpContextAccessor httpContextAccessor)
            : base(httpContextAccessor)
        {
            _mapper = mapper;
            _coreBankingApiService = coreBankingApiService;
        }

        /// <summary>
        /// Get profile summary details
        /// </summary>
        /// <param name="request">LoanAccountMobi4MeRequest parameter</param>
        /// <param name="phoneImei">phoneImei parameter</param>
        /// <returns>The status</returns>
        public async Task<LoanAccountMobi4MeResponse> GetAsync(LoanAccountMobi4MeRequest request, string phoneImei)
        {
            var corebankresponse = await _coreBankingApiService.GetLoanAccountAsync(request.AccountId, GetClientId());
            var response = _mapper.Map<LoanAccountMobi4MeResponse>(corebankresponse);
            return response;
        }
    }
}