﻿using Flt.Mobi4Me.Api.Mobi4Me;
using System.Threading.Tasks;

namespace Flt.Mobi4Me.Api.Services.LoanAccount
{
    public interface ILoanAccount
    {
        Task<LoanAccountMobi4MeResponse> GetAsync(LoanAccountMobi4MeRequest request, string phoneImei);
    }
}