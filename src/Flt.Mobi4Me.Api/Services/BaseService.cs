﻿using Flt.Mobi4Me.Api.Model.KeyCloack;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Text.Json;

namespace Flt.Mobi4Me.Api.Services
{
    public class BaseService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public BaseService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected string GetClientId()
        {
            var claimsIdentity = _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;
            return claimsIdentity.FindFirst("client-id").Value;
        }

        protected bool IsInRole(string roleName)
        {
            try
            {
                var claimsIdentity = _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;
                var realms = claimsIdentity.FindFirst("resource_access").Value;

                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                var response = JsonSerializer.Deserialize<Realms>(realms, options);

                return response.Mobile.Roles.Contains(roleName);
            }
            catch
            {
                return false;
            }
        }
    }
}
