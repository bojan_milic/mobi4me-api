﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;

namespace Flt.Mobi4Me.Api.Helpers
{
    /// <summary>
    /// Represents a Page Link Builder
    /// </summary>
    public class PageLinkBuilder
    {
        private const string PAGE_NUMBER = "pageNo";
        private const string PAGE_SIZE = "pageSize";

        /// <summary>
        /// Constructs a PageLinkBuilder
        /// </summary>
        /// <param name="urlHelper">Url helper</param>
        /// <param name="routeName">Route name</param>
        /// <param name="pageNumber">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="totalRecordCount">Total records</param>
        public PageLinkBuilder(IUrlHelper urlHelper, string routeName, int pageNumber, int pageSize, long totalRecordCount)
        {
            // Determine total number of pages
            var pageCount = totalRecordCount > 0 ? (int)Math.Ceiling(totalRecordCount / (double)pageSize) : 0;

            FirstPage = GererateLink(urlHelper, routeName, 1, pageSize);
            LastPage = GererateLink(urlHelper, routeName, pageCount, pageSize);

            if (pageNumber > 1)
            {
                PreviousPage = GererateLink(urlHelper, routeName, pageNumber - 1, pageSize);
            }

            if (pageNumber < pageCount)
            {
                NextPage = GererateLink(urlHelper, routeName, pageNumber + 1, pageSize);
            }
        }

        /// <summary>
        /// FirstPage link
        /// </summary>
        public Uri FirstPage { get; }

        /// <summary>
        /// LastPage link
        /// </summary>
        public Uri LastPage { get; }

        /// <summary>
        /// NextPage link
        /// </summary>
        public Uri NextPage { get; }

        /// <summary>
        /// PreviousPage link
        /// </summary>
        public Uri PreviousPage { get; }

        /// <summary>
        /// Generates a link
        /// </summary>
        /// <param name="urlHelper">Url helper</param>
        /// <param name="routeName">Route name</param>
        /// <param name="pageNumber">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <returns>A Uri</returns>
        private Uri GererateLink(IUrlHelper urlHelper, string routeName, int pageNumber, int pageSize)
        {
            return new Uri(urlHelper.Link(routeName, new RouteValueDictionary()
            {
                { PAGE_NUMBER, pageNumber },
                { PAGE_SIZE, pageSize }
            }));
        }
    }
}