﻿namespace Flt.Mobi4Me.Api.Helpers
{
    public enum Mobi4MeResponseCode
    {
        Successfull = 1,
        ClientExist = 2,
        UnsendSMS = 4,
        TooManyRequests = 8,
        RequestTimeout = 16,
        InvalidToken = 32,
        InvalidDigitCode = 64,
        AlreadyConfirmed = 128,
        InvalidMobilePhone = 256,
        InternalServerError = 512
    }
}
