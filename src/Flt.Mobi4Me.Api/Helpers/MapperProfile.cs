﻿using AutoMapper;
using Flt.Mobi4Me.Api.CoreSystem;
using Flt.Mobi4Me.Api.Mobi4Me;

namespace Flt.Mobi4Me.Api.Helpers
{
    /// <summary>
    /// Auto mapper profile.
    /// </summary>
    public class MapperProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapperProfile"/> class.
        /// </summary>
        public MapperProfile()
        {
            CreateMap<ClientCoreBankModel, ClientMobi4MeModel>();
            CreateMap<AddressCoreBankModel, AddressMobi4MeModel>();
            CreateMap<BranchCoreBankModel, BranchMobi4MeModel>();
            CreateMap<DepositAccountCoreBankModel, DepositAccountShortMobi4MeModel>();
            CreateMap<LoanAccountCoreBankModel, LoanAccountShortMobi4MeModel>();
            CreateMap<MobileCoreBankModel, MobileMobi4MeModel>();
            CreateMap<PersonalCoreBankModel, PersonalMobi4MeModel>();
            CreateMap<DepositAccountCoreBankModel, DepositAccountMobi4MeModel>();
            CreateMap<FixedDepositCoreBankModel, FixedDepositMobi4MeModel>();
            CreateMap<LoanAccountCoreBankModel, LoanAccountMobi4MeModel>();
            CreateMap<TransactionCoreBankModel, TransactionMobi4MeModel>();

            CreateMap<ClientCoreBankResponse, ProfileSummaryMobi4MeResponse>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.Client.IsActive))
                .ForMember(dest => dest.MainAccountId, opt => opt.MapFrom(src => src.Client.MainAccountId))
                .ForMember(dest => dest.Personal, opt => opt.MapFrom(src => src.Client.Personal))
                .ForMember(dest => dest.DepositAccounts, opt => opt.MapFrom(src => src.Deposits))
                .ForMember(dest => dest.LoanAccounts, opt => opt.MapFrom(src => src.Loans))
                .ForPath(dest => dest.Personal.DisplayName, opt => opt.MapFrom(src => src.Client.Personal.Name))
                .ForPath(dest => dest.Personal.Address, opt => opt.MapFrom(src => src.Client.Personal.Address))
                .ForPath(dest => dest.Personal.Mobile, opt => opt.MapFrom(src => src.Client.Personal.Mobile));

            CreateMap<BranchesCoreBankResponse, BranchesMobi4MeResponse>();
            CreateMap<DepositAccountCoreBankResponse, DepositAccountMobi4MeResponse>();
            CreateMap<LoanAccountCoreBankResponse, LoanAccountMobi4MeResponse>();
            CreateMap<AccountTransactionsCoreBankResponse, AccountTransactionsMobi4MeResponse>();
        }
    }
}
