﻿using Flt.Mobi4Me.Api.Helpers;
using Microsoft.AspNetCore.Builder;

namespace Flt.Mobi4Me.Api
{
    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestResponseLoggingMiddleware>();
        }
    }
}
