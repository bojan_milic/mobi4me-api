﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Flt.Mobi4Me.Api
{
    /// <summary>
    /// Replace version in exact value in path for swagger
    /// </summary>
    public class ReplaceVersionWithExactValueInPath : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var docs = new OpenApiPaths();
            foreach (var path in swaggerDoc.Paths)
            {
                docs.Add(path.Key.Replace("v{version}", swaggerDoc.Info.Version), path.Value);
            }

            swaggerDoc.Paths = docs;
        }
    }
}