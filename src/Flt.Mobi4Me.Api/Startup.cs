using AspNetCoreRateLimit;
using AutoMapper;
using Flt.Mobi4Me.Api.ApiException;
using Flt.Mobi4Me.Api.Helpers;
using Flt.Mobi4Me.Api.Model;
using Flt.Mobi4Me.Api.Services;
using Flt.Mobi4Me.Api.Services.Branches;
using Flt.Mobi4Me.Api.Services.DepositAccount;
using Flt.Mobi4Me.Api.Services.Health;
using Flt.Mobi4Me.Api.Services.LoanAccount;
using Flt.Mobi4Me.Api.Services.OnBoard;
using Flt.Mobi4Me.Api.Services.ProfileSummary;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;

namespace Flt.Mobi4Me.Api
{
    /// <summary>
    /// Represents the startup class
    /// </summary>
    public class Startup
    {
        private readonly IConfigurationRoot _configuration;

        /// <summary>
        /// The Start-up constructor
        /// </summary>
        public Startup()
        {
            _configuration = new ConfigurationBuilder().AddEnvironmentVariables()
                                                       .SetBasePath(Environment.CurrentDirectory)
                                                       .AddJsonFile("appsettings.development.json", true)
                                                       .Build();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container
        /// </summary>
        /// <param name="services">The service collection</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNameCaseInsensitive = false);
            services.AddApiVersioning();

            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 443;
            });

            services.AddMvc(m =>
            {
                m.Filters.Add(new RequireHttpsAttribute());
                m.Filters.Add(new ApiExceptionFilterAttribute());

                // Add XML Content Negotiation
                m.RespectBrowserAcceptHeader = true;
                m.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                m.ReturnHttpNotAcceptable = true;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            .ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problems = new CustomBadRequest(context);

                    return new BadRequestObjectResult(problems);
                };
            });

            // Configure logging service
            services.AddLogging(b => b.AddConsole().AddFilter("Flt.Mobi4Me.Api", LogLevel.Information));
            services.Configure<MailDefinitionModel>(_configuration.GetSection("MailDefinition"));
            services.AddHttpContextAccessor();
            services.AddApplicationInsightsTelemetry();
            services.AddLogging(builder =>
            {
                // Optional: Apply filters to configure LogLevel Trace or above is sent to
                // Application Insights for all categories.
                builder.AddFilter<Microsoft.Extensions.Logging.ApplicationInsights.ApplicationInsightsLoggerProvider>(string.Empty, LogLevel.Debug);
                builder.AddApplicationInsights(_configuration.GetValue<string>("ApplicationInsightsKey"));
            });

            // Dependency injections
            services.AddAutoMapper(typeof(MapperProfile));
            services.AddSingleton<ICoreBankingApiService, CoreBankingApiService>();
            services.AddSingleton<IHealth, Health>();
            services.AddSingleton<IBranches, Branches>();
            services.AddSingleton<IProfileSummary, ProfileSummary>();
            services.AddSingleton<IPotentialClient, PotentialClient>();
            services.AddSingleton<IOnBoard, OnBoard>();
            services.AddSingleton<ILoanAccount, LoanAccount>();
            services.AddSingleton<IDepositAccount, DepositAccount>();
            services.AddSingleton<IAccountTransactions, AccountTransactions>();
            services.AddSingleton<IKeyCloackService, KeyCloackService>();

            // needed to store rate limit counters and ip rules
            services.AddMemoryCache();
            // load general configuration from appsettings.json
            services.Configure<IpRateLimitOptions>(_configuration.GetSection("IpRateLimiting"));
            // inject counter and rules stores
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();

            services.AddScoped(f =>
            {
                var actionContext = f.GetService<IActionContextAccessor>().ActionContext;
                var factory = f.GetService<IUrlHelperFactory>();

                return factory.GetUrlHelper(actionContext);
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "Mobi4Me API v1",
                        Description = "Mobi4Me API v1 API Description"
                    });

                // Apply the filters
                options.OperationFilter<RemoveVersionFromParameter>();
                options.DocumentFilter<ReplaceVersionWithExactValueInPath>();

                // Ensure the routes are added to the right Swagger doc
                options.DocInclusionPredicate((_, __) => true);
                options.SchemaFilter<SchemaFilter>();

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }
                });

                options.DescribeAllParametersInCamelCase();
                options.EnableAnnotations();
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.Authority = _configuration.GetValue<string>("AuthAuthority");
                o.Audience = "account";
                o.Events = new JwtBearerEvents()
                {
                    OnAuthenticationFailed = c =>
                    {
                        c.NoResult();

                        c.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        c.Response.ContentType = "text/plain";
                        return c.Response.WriteAsync(c.Exception.ToString());
                    }
                };
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        /// </summary>
        /// <param name="app">The application builder</param>
        /// <param name="env">The host environment</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection()
               .UseRouting()
               .UseAuthentication()
               .UseHsts()
               .UseAuthorization()
               .UseIpRateLimiting()
               .UseRequestResponseLogging()
               .UseEndpoints(endpoints => endpoints.MapControllers())
               .UseSwagger()
               .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mobi4Me API v1"));
        }
    }
}